import numpy as np

SUDOKU_LENGTH = 9
SQRT_LENGTH = 3
EMPTY = 0


def possible_position(number: int, x_coord: int, y_coord: int) -> bool:
    """
    function get number, and check is his position is ok
    :param number: a target number to check
    :param x_coord: a x coordination
    :param y_coord: a y coordination
    :return: True - ok, else - False
    """

    # check if the number in the column/row -> False
    for index in range(SUDOKU_LENGTH):
        if board[y_coord][index] == number or board[index][x_coord] == number:
            return False
    
    # check if the number in the square -> False
    x_sqr = (x_coord // SQRT_LENGTH) * SQRT_LENGTH
    y_sqr = (y_coord // SQRT_LENGTH) * SQRT_LENGTH

    for i in range(SQRT_LENGTH):
        for j in range(SQRT_LENGTH):
            if board[y_sqr + i][x_sqr + j] == number:
                return False

    return True


def solution():
    """
    function solve the sudoku
    :return: None
    """

    # run across the cols
    for y in range(SUDOKU_LENGTH):

        # run across the rows in the col
        for x in range(SUDOKU_LENGTH):

            # if the position is empty - generate number
            if board[y][x] == EMPTY:

                # get all the possible numbers (1 - 9)
                for number in range(1, 10):

                    # if the number is valid to this place - put him, and then recursively start to trace the next empty number
                    if possible_position(number, x, y):
                        board[y][x] = number

                        # try to get solved board from the function if it is, return him
                        b = solution()

                        if b:
                            return b
                        else:
                            # if the position is not good, reset the point
                            board[y][x] = EMPTY

                # if all the generate numbers (1 - 9) not goods, this is not the path for the solution, so return none (break)
                return

    return board.copy()


board = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9]
]


def main():
    b = solution()
    print(np.matrix(b))
    

if __name__ == '__main__':
    main()
